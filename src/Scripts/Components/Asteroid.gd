extends RigidBody2D

# Public variables
export(float) var speed = 100

# Private variables
onready var _collision_node = get_node("Collision")
onready var _sprite_node = get_node("Sprite")

# PoolableResource variables
var is_pool_initialized = false
var is_pool_added_to_tree_on_init = false

# Engine methods
func _ready():
	reset()

func _integrate_forces(state):
	# Only go into the screen
	var screen_ratio = 0.1
	var real_viewport_size = Engine.get_main_loop().get_root().get_size_override()
	var current_pos = get_global_position()
	var new_pos = get_global_position()

	if current_pos.x < 0 - (real_viewport_size.x * 0.1):
		new_pos.x = real_viewport_size.x  + (real_viewport_size.x * 0.1) - 1
	
	if current_pos.x > real_viewport_size.x + (real_viewport_size.x * 0.1):
		new_pos.x = -(real_viewport_size.x * 0.1) + 1
	
	if current_pos.y < 0 - (real_viewport_size.y * 0.1):
		new_pos.y = real_viewport_size.y  + (real_viewport_size.y * 0.1) - 1
	
	if current_pos.y > real_viewport_size.y + (real_viewport_size.y * 0.1):
		new_pos.y = -(real_viewport_size.y * 0.1) + 1
	
	if current_pos.x != new_pos.x or current_pos.y != new_pos.y:
		set_global_position(new_pos)
		
# Public methods
func reset():
	set_process(false)
	set_mode(RigidBody2D.MODE_STATIC)
	set_contact_monitor(false)
	set_linear_velocity(Vector2(0.0,0.0))
	set_collision_mask(0)
	set_collision_layer(0)
	set_sleeping(true)
	_sprite_node.set_visible(false)
	
	_collision_node.set_disabled(true)

func run():
	set_process(true)
	set_mode(RigidBody2D.MODE_RIGID)
	set_contact_monitor(true)
	set_collision_mask_bit(0, true)
	set_collision_layer_bit(0, true)
	set_sleeping(false)
	_sprite_node.set_visible(true)
	
	_collision_node.set_disabled(false)
	
	set_linear_velocity(Vector2(rand_range(0.0, 1.0), rand_range(0.0, 1.0)).normalized() * speed)
	
# PoolableResource methods
func pool_initialize():
	is_pool_initialized = true

func pool_free():
	reset()
	
	if not is_pool_added_to_tree_on_init:
		get_parent().remove_child(self)

	is_pool_initialized = false