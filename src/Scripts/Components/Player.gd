extends RigidBody2D

# Public variables
export(float) var speed = 100

# Private variables
onready var _collision_node = get_node(@"Collision")
onready var _sprite_node = get_node(@"Sprite")
onready var _area_node = get_node(@"Area2D")
onready var _area_collision = get_node(@"Area2D/Area_Collision")

# Engine methods
func _integrate_forces(state):
	var velocity = Vector2(0.0, 0.0)
	var delta = state.get_step()

	if Input.is_action_pressed("ui_left"):
		velocity += Vector2(-1.0, 0.0)
		
	if Input.is_action_pressed("ui_right"):
		velocity += Vector2(1.0, 0.0)
		
	if Input.is_action_pressed("ui_up"):
		velocity += Vector2(0.0, -1.0)
		
	if Input.is_action_pressed("ui_down"):
		velocity += Vector2(0.0, 1.0)
	
	set_linear_velocity(velocity.normalized() * speed)
	
	# Only go into the screen
	var screen_ratio = 0.1
	var real_viewport_size = Engine.get_main_loop().get_root().get_size_override()
	var current_pos = get_global_position()
	var new_pos = get_global_position()

	if current_pos.x < 0 - (real_viewport_size.x * 0.1):
		new_pos.x = real_viewport_size.x  + (real_viewport_size.x * 0.1) - 1
	
	if current_pos.x > real_viewport_size.x + (real_viewport_size.x * 0.1):
		new_pos.x = -(real_viewport_size.x * 0.1) + 1
	
	if current_pos.y < 0 - (real_viewport_size.y * 0.1):
		new_pos.y = real_viewport_size.y  + (real_viewport_size.y * 0.1) - 1
	
	if current_pos.y > real_viewport_size.y + (real_viewport_size.y * 0.1):
		new_pos.y = -(real_viewport_size.y * 0.1) + 1
	
	if current_pos.x != new_pos.x or current_pos.y != new_pos.y:
		set_global_position(new_pos)
	