extends Node

# Constants
const MAX_ELEMENTS = 100
const STEP = 10

# Private variables
onready var _elements_layer_node = get_node(@"CanvasLayer_Game/Elements_Layer")
onready var _button_reset_node = get_node(@"CanvasLayer_HUD/HBoxContainer_Control/Button_Reset")
onready var _button_substract_node = get_node(@"CanvasLayer_HUD/HBoxContainer_Control/Button_Substract")
onready var _button_add_node = get_node(@"CanvasLayer_HUD/HBoxContainer_Control/Button_Add")
onready var _label_elements_node = get_node(@"CanvasLayer_HUD/HBoxContainer_Control/Label_Elements")
onready var _label_fps_value = get_node(@"CanvasLayer_HUD/HBoxContainer_FPS/Label_FPS_Value")
onready var _button_pool_initialize_node = get_node(@"CanvasLayer_HUD/HBoxContainer_Control/Button_Pool_Initialize")
onready var _checkbox_addtotree_node = get_node(@"CanvasLayer_HUD/HBoxContainer_Control/CheckBox_AddToTree")

var _asteroids_pool = null
var _active_elements = []

# Instances
const SceneResourcePooler = preload("res://Scripts/Core/SceneResourcePooler.gd")

# Engine methods
func _init():
	OS.set_window_size(Vector2(1024.0, 600.0))

func _ready():
	# Connect button signals
	_button_add_node.connect("pressed", self, "add_elements")
	_button_substract_node.connect("pressed", self, "remove_elements")
	_button_reset_node.connect("pressed", self, "reset_elements")
	_button_pool_initialize_node.connect("pressed", self, "pool_initialize")

	_button_add_node.set_disabled(true)
	_button_substract_node.set_disabled(true)
	_button_reset_node.set_disabled(true)
	
func _process(delta):
	_label_fps_value.set_text(str(Performance.get_monitor(Performance.TIME_FPS)))
	_label_elements_node.set_text(str(_active_elements.size()))
	
# Public methods
func pool_initialize():
	# Initialize pooler
	_asteroids_pool = SceneResourcePooler.new("res://Assets/Components/Asteroid.tscn", _elements_layer_node.get_path(), MAX_ELEMENTS, _checkbox_addtotree_node.is_pressed())
	
	_checkbox_addtotree_node.set_disabled(true)
	_button_pool_initialize_node.set_disabled(true)
	_button_add_node.set_disabled(false)
	_button_substract_node.set_disabled(false)
	_button_reset_node.set_disabled(false)

func get_random_position():
	var width = randi() % int(Engine.get_main_loop().get_root().get_size_override().x)
	var height  = randi() % int(Engine.get_main_loop().get_root().get_size_override().y)
	
	return Vector2(width, height)

func add_elements():
	if _active_elements.size() == MAX_ELEMENTS:
		return

	for i in range(1,STEP + 1):
		_active_elements.append(_asteroids_pool.get_new_resource())
		_active_elements.back().set_global_position(get_random_position())
		_active_elements.back().run()

func remove_elements():
	if _active_elements.size() == 0:
		return
		
	for i in range(1, STEP + 1):
		_active_elements.back().pool_free()
		_active_elements.pop_back()

func reset_elements():
	for element in _active_elements:
		element.pool_free()
		
	_active_elements.clear()
	
	